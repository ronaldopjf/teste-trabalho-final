<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="col-md-8">

    <div class="card card-user">
        <div class="image stretch">
            <img src="./assets/img/ganhando1.jpg" alt="...">
        </div>
        <div class="card-body">
            <div class="author">
                <a href="#">
                    <img class="avatar border-gray" src="./assets/img/podium.png" alt="...">
                    <c:if test="${requestScope.status == null}">
                        <h3 class="title">Parab�ns </h3>                        
                    </c:if>
                    <c:if test="${requestScope.status != null}">
                        <h3 class="title">${requestScope.partida.dataFormatada} </h3>                        
                    </c:if>
                </a>
                <p class="description">
                    Tempo :: ${requestScope.partida.tempo} minutos
                </p>
            </div>
            <p class="description text-center">
            <h1  class="text-center">
                Acertos :: ${requestScope.partida.acertos}<br />
                Erros :: ${requestScope.partida.erros}<br />
                Pontua��o :: Cr$ ${requestScope.partida.bonificacaoFormatada}<br />
            </h1>
            </p>
        </div>
        <div class="card-footer">
            <hr>
            <div class="button-container">
                <c:if test="${requestScope.status != null}">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-12 ml-auto mr-auto">
                            <div class="card"  style="background-color: #f4f3ef">
                                <div class="card-header">
                                    <h4 class="card-title">Quest�es</h4>
                                </div>
                                <div class="card-body">
                                    <ul class="list-unstyled team-members">
                                        <c:forEach items="${requestScope.partida.jogoList}" var="j" varStatus="loop">
                                            <li>
                                                <div class="row">
                                                    <div class="col-md-2 col-2">
                                                        <div class="avatar" style="width: 85px; height: 64px; border: none;">
                                                            ${loop.index + 1}
                                                            <c:set var="img" value="${j.correct?'ok1.png':'nok1.png'}" scope="request"/>
                                                            <img src="./assets/img/${img}" alt="Circle Image" class="img-circle img-no-padding img-responsive">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-7 col-7">
                                                        <h3> ${j.valor1} ${j.operador.operador} ${j.valor2} = ${j.resultado}</h3>
                                                    </div>
                                                    <div class="col-md-3 col-3 text-right">
                                                        <h2>${j.resposta} </h2>
                                                    </div>
                                                </div>
                                            </li>
                                        </c:forEach>
                                        <!--                                    
                                                                            <li>
                                                                                <div class="row">
                                                                                    <div class="col-md-2 col-2">
                                                                                        <div class="avatar">
                                                                                            <img src="./assets/img/nok1.png" alt="Circle Image" class="img-circle img-no-padding img-responsive">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-7 col-7">
                                                                                        <h1> 30 - 54</h1>
                                                                                        <br>
                                                                                        <span class="text-muted">
                                                                                            <small>10</small>
                                                                                        </span>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-3 text-right">
                                                                                        <h1>25</h1>
                                                                                    </div>
                                                                                </div>
                                                                            </li>
                                                                            
                                        -->
                                    </ul>
                                </div>
                            </div>                        
                        </div>
                    </div>
                </c:if>
                <div class="row">
                    <div class="col-lg-10 col-md-10 col-10 ml-auto mr-auto">
                        <form action="?ac=competicao" method="POST">
                            <!--<input type="hidden" name="op" value="jog"/>-->
                            <h5>
                                <button type="submit" class="btn btn-primary btn-round">Minhas Competi��es</button>
                            </h5>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
