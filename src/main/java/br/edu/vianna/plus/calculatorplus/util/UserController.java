/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.util;

import br.edu.vianna.plus.calculatorplus.dao.UsuarioDao;
import br.edu.vianna.plus.calculatorplus.domain.Usuario;

/**
 *
 * @author aluno
 */
public class UserController {

    public UsuarioDao uDao;

    public boolean usuarioLogin(String login, String senha) {

        Usuario user = uDao.findByLoginAndSenha(login, senha);

        if (user == null) {

            return false;
        } else {
            return true;

        }
    }

}
