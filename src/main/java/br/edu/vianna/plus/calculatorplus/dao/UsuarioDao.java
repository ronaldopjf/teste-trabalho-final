/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.dao;

import br.edu.vianna.plus.calculatorplus.controller.action.dto.RankingDTO;
import br.edu.vianna.plus.calculatorplus.domain.Usuario;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author daves
 */
public class UsuarioDao extends DaoGenerics {

    public long getAllUsers() {

        Query q = conection.createQuery("select count(u) from Usuario u", Long.class);

        return (long) q.getSingleResult();
    }

    public void save(Usuario user) {
        conection.getTransaction().begin();
        conection.persist(user);
        conection.getTransaction().commit();
    }

    public Usuario findByLoginAndSenha(String login, String senha) {

        Query q = conection.createQuery("select u from Usuario u where u.login= :log and u.senha= :sen");

        q.setParameter("log", login);
        q.setParameter("sen", senha);
        try {
            return (Usuario) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    public List<RankingDTO> getRanking() {

        Query q = conection.createQuery("select p.usuario.nome, sum(p.bonificacao) as tot, sum(p.tempo), count(p) "
                + "from Partida p group by p.usuario.nome order by tot desc");

        List<Object[]> lista = q.getResultList();
        List<RankingDTO> retorno = new ArrayList<>();
        for (Object[] obj : lista) {
            RankingDTO rd = new RankingDTO((String)obj[0], (Double)obj[1], (Long)obj[2], (Long)obj[3]);
            retorno.add(rd);

        }

        return retorno;
    }

}
