/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.controller.action;

import br.edu.vianna.plus.calculatorplus.controller.faces.IActionCommand;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author daves
 */
public class ViewCadastroUserAction implements IActionCommand {

    public ViewCadastroUserAction() {
    }

    @Override
    public boolean ehSegura() {
        return false;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        RequestDispatcher rd = request.getRequestDispatcher("/template.jsp?page=cadUser");

        rd.forward(request, response);

    }

}
