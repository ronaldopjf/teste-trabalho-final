/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.controller;

import br.edu.vianna.plus.calculatorplus.controller.action.ViewCompeticaoAction;
import br.edu.vianna.plus.calculatorplus.controller.action.LogoutAction;
import br.edu.vianna.plus.calculatorplus.controller.action.SaveUserAction;
import br.edu.vianna.plus.calculatorplus.controller.action.ViewCadastroUserAction;
import br.edu.vianna.plus.calculatorplus.controller.action.ViewDashboardAction;
import br.edu.vianna.plus.calculatorplus.controller.action.ViewDetalheCompeticaoAction;
import br.edu.vianna.plus.calculatorplus.controller.action.ViewLoginAction;
import br.edu.vianna.plus.calculatorplus.controller.action.ViewRankingAction;
import br.edu.vianna.plus.calculatorplus.controller.action.ajaxDataGraphAction;
import br.edu.vianna.plus.calculatorplus.controller.faces.IActionCommand;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author daves
 */
@WebServlet(name = "CentralControler", urlPatterns = {"/app/"})
public class CentralController extends HttpServlet {

    private static Map<String, IActionCommand> comandos;

    static {
        comandos = new HashMap<>();
//        comandos.put(null, new ViewHomeAction());
//        comandos.put("home", new ViewHomeAction());
        comandos.put(null, new ViewDashboardAction());
        comandos.put("home", new ViewDashboardAction());
        comandos.put("dashboard", new ViewDashboardAction());
        comandos.put("ajaxData", new ajaxDataGraphAction());
        comandos.put("cadastro", new ViewCadastroUserAction());
        comandos.put("saveUser", new SaveUserAction());
        comandos.put("login", new ViewLoginAction());
        comandos.put("competicao", new ViewCompeticaoAction());
        comandos.put("detalhesPart", new ViewDetalheCompeticaoAction());
        comandos.put("ranking", new ViewRankingAction());
        comandos.put("logout", new LogoutAction());
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String acao = request.getParameter("ac");

        try {

            if (!comandos.get(acao).ehSegura()) {
                comandos.get(acao).execute(request, response);
            } else if (comandos.get(acao).ehSegura()
                    && request.getSession().getAttribute("user") != null) {
                comandos.get(acao).execute(request, response);
            } else {
                request.setAttribute("msgAviso", "login necessário");
                comandos.get("login").execute(request, response);
            }

        } catch (Exception ex) {

            request.setAttribute("msgErro", "ERRO ::" + ex.getMessage() + "<br />Tipo de erro ::" + ex.getClass().getName());
            
            RequestDispatcher rd = request.getRequestDispatcher("/template.jsp");
            rd.forward(request, response);

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
