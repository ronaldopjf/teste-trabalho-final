/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mock;

import br.edu.vianna.plus.calculatorplus.dao.JogoDao;
import br.edu.vianna.plus.calculatorplus.dao.PartidaDao;
import br.edu.vianna.plus.calculatorplus.dao.UsuarioDao;
import br.edu.vianna.plus.calculatorplus.domain.EOperator;
import br.edu.vianna.plus.calculatorplus.domain.Jogo;
import br.edu.vianna.plus.calculatorplus.domain.Partida;
import br.edu.vianna.plus.calculatorplus.domain.Usuario;
import org.junit.Assert;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import br.edu.vianna.plus.calculatorplus.util.UserController;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.util.Date;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author aluno
 */
public class MockitoTest {

    @Test
    public void mockUsuarioGetCidadeTest() {

        Usuario userMock = mock(Usuario.class);
        userMock.setCidade("Juiz de Fora");
        when(userMock.getCidade()).thenReturn("Pindamonhangaba");
        assertEquals(userMock.getCidade(), "Pindamonhangaba");
    }

    @Test
    public void mockPartidaDaoTest() throws SQLException {

        PartidaDao mockPartidaDao = mock(PartidaDao.class);

        when(mockPartidaDao.getAllBonus()).thenReturn(1542.5);
        when(mockPartidaDao.userJaCompetiuHoje(new Usuario())).thenReturn(true);
        when(mockPartidaDao.getPartida(10)).thenReturn(new Partida(10));
        when(mockPartidaDao.getPartidaIniciada(new Usuario(12))).thenReturn(new Partida(15));
        when(mockPartidaDao.iniciarPartida(new Usuario(14))).thenReturn(new Partida(17));

        assertTrue(mockPartidaDao.getAllBonus() == 1542.5);
        assertTrue(mockPartidaDao.userJaCompetiuHoje(new Usuario()));
        assertEquals(mockPartidaDao.getPartida(10).getId().toString(), "10");
        assertEquals(mockPartidaDao.getPartidaIniciada(new Usuario(12)).getId().toString(), "15");
        assertEquals(mockPartidaDao.iniciarPartida(new Usuario(14)).getId().toString(), "17");
    }

    @Test
    public void mockUsuarioDaoTest() {

        UsuarioDao ud = mock(UsuarioDao.class);
        when(ud.findByLoginAndSenha("ze", "123")).thenReturn(new Usuario());
        when(ud.getAllUsers()).thenReturn(Long.parseLong("704"));

        UserController t = new UserController();
        t.uDao = ud;

        boolean achou = t.usuarioLogin("ze", "123");

        Assert.assertTrue(achou);
        Assert.assertEquals(t.uDao.getAllUsers(), Long.parseLong("704"));

    }

    @Test
    public void mockUsuarioTest() {

        Usuario u = mock(Usuario.class);

        when(u.getNome()).thenReturn("Pedrin");
        when(u.getCidade()).thenReturn("Juiz de Fora");
        when(u.getDataNascimento()).thenReturn(new Date());
        when(u.getEmail()).thenReturn("pedrin@gmail.com");
        when(u.getLogin()).thenReturn("ped");
        when(u.getSenha()).thenReturn("pedped");

        Assert.assertEquals(u.getNome(), "Pedrin");
        Assert.assertEquals(u.getCidade(), "Juiz de Fora");
        Assert.assertEquals(u.getDataNascimento().toString(), new Date().toString());
        Assert.assertEquals(u.getEmail(), "pedrin@gmail.com");
        Assert.assertEquals(u.getLogin(), "ped");
        Assert.assertEquals(u.getSenha(), "pedped");
    }

    @Test
    public void mockUsuarioValidacaoTest() {

        Usuario u = mock(Usuario.class);

        when(u.validacpf("123")).thenReturn(true);
        when(u.validacpf("")).thenReturn(false);
        when(u.loginValida()).thenReturn(true);
        when(u.senhaValida()).thenReturn(true);

        Assert.assertEquals(u.validacpf("123"), true);
        Assert.assertEquals(u.validacpf(""), false);
        Assert.assertEquals(u.loginValida(), true);
        Assert.assertEquals(u.senhaValida(), true);
    }

    @Test
    public void mockPartidaTest() {

        Partida p = mock(Partida.class);

        when(p.getAcertos()).thenReturn(20);
        when(p.getBonificacao()).thenReturn(25.5);
        when(p.getBonificacaoFormatada()).thenReturn("25.5");
        when(p.getData()).thenReturn(new Date());
        when(p.getDataFormatada()).thenReturn("20/10/2019");
        when(p.getErros()).thenReturn(15);
        when(p.getTempo()).thenReturn(Long.parseLong("9"));
        when(p.getUsuario()).thenReturn(new Usuario(78));

        Assert.assertEquals(p.getAcertos(), 20);
        Assert.assertEquals(p.getBonificacao(), 25.5, 0);
        Assert.assertEquals(p.getBonificacaoFormatada(), "25.5");
        Assert.assertEquals(p.getData().toString(), new Date().toString());
        Assert.assertEquals(p.getDataFormatada(), "20/10/2019");
        Assert.assertEquals(p.getErros(), 15);
        Assert.assertEquals(p.getTempo(), Long.parseLong("9"));
        Assert.assertEquals(p.getUsuario().getId().toString(), "78");
    }

    @Test
    public void mockJogoTest() {

        Jogo j = mock(Jogo.class);

        when(j.getBonus()).thenReturn(20.1);
        when(j.getIdjogo()).thenReturn(27);
        when(j.getOperador()).thenReturn(EOperator.soma);
        when(j.getPartida()).thenReturn(new Partida(54));
        when(j.getResposta()).thenReturn(44.6);
        when(j.getResultado()).thenReturn(28.9);
        when(j.getValor1()).thenReturn(76.9);
        when(j.getValor2()).thenReturn(54.2);

        Assert.assertEquals(j.getBonus(), 20.1, 0);
        Assert.assertEquals(j.getIdjogo().toString(), "27");
        Assert.assertEquals(j.getOperador(), EOperator.soma);
        Assert.assertEquals(j.getPartida().getId().toString(), "54");
        Assert.assertEquals(j.getResposta(), 44.6, 0);
        Assert.assertEquals(j.getResultado(), 28.9, 0);
        Assert.assertEquals(j.getValor1(), 76.9, 0);
        Assert.assertEquals(j.getValor2(), 54.2, 0);
    }

    @Test
    public void mockJogoValidacaoTest() {

        Jogo j = mock(Jogo.class);

        when(j.isCorrect()).thenReturn(true);
        when(j.estaCerto()).thenReturn(false);
        when(j.bonusInicial()).thenReturn(new BigDecimal(BigInteger.ONE));

        Assert.assertEquals(j.isCorrect(), true);
        Assert.assertEquals(j.estaCerto(), false);
        Assert.assertEquals(j.bonusInicial(), new BigDecimal(BigInteger.ONE));
    }

    @Test
    public void mockJogoDaoTest() {

        JogoDao mockJogoDao = mock(JogoDao.class);

        when(mockJogoDao.getAllAcertos()).thenReturn(Long.parseLong("625"));
        when(mockJogoDao.getAllAcertos(11)).thenReturn(Long.parseLong("456"));
        when(mockJogoDao.getAllErros()).thenReturn(Long.parseLong("654"));

        assertEquals(mockJogoDao.getAllAcertos(), Long.parseLong("625"));
        assertEquals(mockJogoDao.getAllAcertos(11), Long.parseLong("456"));
        assertEquals(mockJogoDao.getAllErros(), Long.parseLong("654"));
    }

}
