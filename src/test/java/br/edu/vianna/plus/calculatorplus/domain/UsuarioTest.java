/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author iranf
 */

public class UsuarioTest {
    
    public UsuarioTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void senhaValidaTest() {
        Usuario instance = new Usuario();
        instance.setSenha("123A@a");
        boolean expResult = true;
        boolean result = instance.senhaValida();
        assertEquals(expResult, result);
    }

    @Test
    public void senhaNaoValidaTest() {
        Usuario instance = new Usuario();
        instance.setSenha("123");
        boolean expResult = false;
        boolean result = instance.senhaValida();
        assertEquals(expResult, result);

    }
    
    @Test
    public void senhaNaoValida2Test() {
        Usuario instance = new Usuario();
        instance.setSenha("123456");
        boolean expResult = false;
        boolean result = instance.senhaValida();
        assertEquals(expResult, result);

    }
    
    @Test
    public void senhaNaoValida3Test() {
        Usuario instance = new Usuario();
        instance.setSenha("1234567890");
        boolean expResult = false;
        boolean result = instance.senhaValida();
        assertEquals(expResult, result);

    }

    @Test
    public void senhaVazioTest() {
        Usuario instance = new Usuario();
        instance.setSenha("");
        boolean expResult = false;
        boolean result = instance.senhaValida();
        assertEquals(expResult, result);

    }

    @Test
    public void loginValidaTest() {
        Usuario instance = new Usuario();
        instance.setLogin("pedped");//entre seis e quinze caracteres sem espaço
        boolean expResult = true;
        boolean result = instance.loginValida();
        assertEquals(expResult, result);

    }

    @Test
    public void loginNaoValidaTest() {
        Usuario instance = new Usuario();
        instance.setLogin("ped ped");
        boolean expResult = false;
        boolean result = instance.loginValida();
        assertEquals(expResult, result);

    }
    
    @Test
    public void loginNaoValida2Test() {
        Usuario instance = new Usuario();
        instance.setLogin("ped");
        boolean expResult = false;
        boolean result = instance.loginValida();
        assertEquals(expResult, result);

    }
    
    @Test
    public void loginNaoValida3Test() {
        Usuario instance = new Usuario();
        instance.setLogin("123456789101112131415");
        boolean expResult = false;
        boolean result = instance.loginValida();
        assertEquals(expResult, result);

    }

    @Test
    public void loginVazioTest() {
        Usuario instance = new Usuario();
        instance.setLogin("");
        boolean expResult = false;
        boolean result = instance.loginValida();
        assertEquals(expResult, result);

    }
    
    @Test
    public void getSetBasicoTest() {
        Usuario instance = new Usuario();
        instance.setNome("Pedrin");
        instance.setLogin("pedrin");
        instance.setEmail("pedrin@gmail.com");
        instance.setSenha("pedriN@123");
        instance.setCidade("Juiz de Fora");
        instance.setDataNascimento(new Date());
        instance.setId(5);        
        
        instance.setPartidaList(new ArrayList<>());
        instance.getPartidaList().add(new Partida(5, new Date(), 20.0, Long.parseLong("12")));
        instance.getPartidaList().add(new Partida(6, new Date(), 70.0, Long.parseLong("30")));
        
        assertEquals("Pedrin", instance.getNome());
        assertEquals("pedrin", instance.getLogin());
        assertEquals("pedrin@gmail.com", instance.getEmail());
        assertEquals("pedriN@123", instance.getSenha());
        assertEquals("Juiz de Fora", instance.getCidade());
        assertEquals(new Date().toString(), instance.getDataNascimento().toString());
        assertEquals("5", instance.getId().toString());
        
        assertNotNull(instance.getPartidaList());
        assertEquals(2, instance.getPartidaList().size());        
        assertEquals("5", instance.getPartidaList().get(0).getId().toString());        
        assertEquals(20.0, instance.getPartidaList().get(0).getBonificacao(), 0); 
        assertEquals("6", instance.getPartidaList().get(1).getId().toString());        
        assertEquals(70.0, instance.getPartidaList().get(1).getBonificacao(), 0);
        
    }
    
    @Test
    public void construtorVazioTest() {
        Usuario instance = new Usuario();       
        
        assertNotNull(instance);
        assertNull(instance.getNome());
        assertNull(instance.getLogin());
        assertNull(instance.getEmail());
        assertNull(instance.getSenha());
        assertNull(instance.getCidade());
        assertNull(instance.getDataNascimento());
        assertNull(instance.getId());
        
    }
    
    @Test
    public void construtorIdTest() {
        Usuario instance = new Usuario(5);       
        
        assertNotNull(instance);
        assertNull(instance.getNome());
        assertNull(instance.getLogin());
        assertNull(instance.getEmail());
        assertNull(instance.getSenha());
        assertNull(instance.getCidade());
        assertNull(instance.getDataNascimento());
        assertEquals("5", instance.getId().toString());
        
    }
    
    @Test
    public void construtorCheioTest() {
        Usuario instance = new Usuario(5, "Pedrin", "pedrin", "pedrin@gmail.com", "pedriN@123", "Juiz de Fora", 
                new Date());       
        
        instance.setPartidaList(new ArrayList<>());
        instance.getPartidaList().add(new Partida(5, new Date(), 20.0, Long.parseLong("12")));
        instance.getPartidaList().add(new Partida(6, new Date(), 70.0, Long.parseLong("30")));
        
        assertNotNull(instance);
        assertEquals("Pedrin", instance.getNome());
        assertEquals("pedrin", instance.getLogin());
        assertEquals("pedrin@gmail.com", instance.getEmail());
        assertEquals("pedriN@123", instance.getSenha());
        assertEquals("Juiz de Fora", instance.getCidade());
        assertEquals(new Date().toString(), instance.getDataNascimento().toString());
        assertEquals("5", instance.getId().toString());
        
        assertNotNull(instance.getPartidaList());
        assertEquals(2, instance.getPartidaList().size());        
        assertEquals("5", instance.getPartidaList().get(0).getId().toString());        
        assertEquals(20.0, instance.getPartidaList().get(0).getBonificacao(), 0); 
        assertEquals("6", instance.getPartidaList().get(1).getId().toString());        
        assertEquals(70.0, instance.getPartidaList().get(1).getBonificacao(), 0);
    }
}
