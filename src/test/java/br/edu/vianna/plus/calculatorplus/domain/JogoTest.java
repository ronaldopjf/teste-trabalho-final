/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.domain;

import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author iranf
 */
public class JogoTest {
    
    public JogoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void estaCertoTest() {
        Jogo instance = new Jogo();
        instance.setResposta(10.5);
        instance.setResultado(10.5);

        assertEquals(true, instance.estaCerto());

    }

    @Test
    public void naoEstaCertoTest() {
        Jogo instance = new Jogo();
        instance.setResposta(10.5);
        instance.setResultado(10);

        assertEquals(false, instance.estaCerto());

    }

    @Test
    public void isCorrectTest() {
        Jogo instance = new Jogo();
        instance.setResposta(10.0);
        instance.setResultado(10.0);
        assertEquals(true, instance.isCorrect());

    }

    @Test
    public void notIsCorrectTest() {
        Jogo instance = new Jogo();
        instance.setResposta(11.0);
        instance.setResultado(10.0);
        assertEquals(false, instance.isCorrect());

    }
    
    @Test
    public void construtorVazioTest() {
        Jogo instance = new Jogo();      
        
        assertNotNull(instance);
        assertNull(instance.getIdjogo());
        assertEquals(0.0, instance.getBonus(), 0);
        assertNull(instance.getOperador());
        assertNull(instance.getPartida());
        assertEquals(0.0, instance.getResposta(), 0);
        assertEquals(0.0, instance.getResultado(), 0);
        assertEquals(0.0, instance.getValor1(), 0);
        assertEquals(0.0, instance.getValor2(), 0);
        
    }
    
    @Test
    public void construtorIdTest() {
        Jogo instance = new Jogo(1);      
        
        assertNotNull(instance);
        assertEquals("1", instance.getIdjogo().toString());
        assertEquals(0.0, instance.getBonus(), 0);
        assertNull(instance.getOperador());
        assertNull(instance.getPartida());
        assertEquals(0.0, instance.getResposta(), 0);
        assertEquals(0.0, instance.getResultado(), 0);
        assertEquals(0.0, instance.getValor1(), 0);
        assertEquals(0.0, instance.getValor2(), 0);
        
    }
    
    @Test
    public void construtorCheioTest() {
        Jogo instance = new Jogo(1, 10.0, 20.0, EOperator.soma, 30.0, 35.0, 15.0);      
        
        instance.setPartida(new Partida(5, new Date(), 20.0, Long.parseLong("12")));
        
        assertNotNull(instance);
        assertEquals("1", instance.getIdjogo().toString());
        assertEquals(15.0, instance.getBonus(), 0);
        assertEquals(EOperator.soma, instance.getOperador());
        assertEquals(35.0, instance.getResposta(), 0);
        assertEquals(30.0, instance.getResultado(), 0);
        assertEquals(10.0, instance.getValor1(), 0);
        assertEquals(20.0, instance.getValor2(), 0);
        
        assertNotNull(instance.getPartida());
        assertEquals("5", instance.getPartida().getId().toString());
        assertEquals(20.0, instance.getPartida().getBonificacao(), 0);
    }
    
    @Test
    public void getSetBasicoTest() {
        Jogo instance = new Jogo(1, 10.0, 20.0, EOperator.soma, 30.0, 35.0, 15.0);
        
        instance.setIdjogo(1);
        instance.setValor1(10.0);
        instance.setValor2(20.0);
        instance.setOperador(EOperator.soma);
        instance.setResultado(30.0);
        instance.setResposta(35.0);
        instance.setBonus(15.0);
        
        instance.setPartida(new Partida(5, new Date(), 20.0, Long.parseLong("12")));
        
        assertNotNull(instance);
        assertEquals("1", instance.getIdjogo().toString());
        assertEquals(15.0, instance.getBonus(), 0);
        assertEquals(EOperator.soma, instance.getOperador());
        assertEquals(35.0, instance.getResposta(), 0);
        assertEquals(30.0, instance.getResultado(), 0);
        assertEquals(10.0, instance.getValor1(), 0);
        assertEquals(20.0, instance.getValor2(), 0);
        
        assertNotNull(instance.getPartida());
        assertEquals("5", instance.getPartida().getId().toString());
        assertEquals(20.0, instance.getPartida().getBonificacao(), 0);
    }
    
    @Test
    public void bonusOperacaoTest() {
        Jogo instance = new Jogo();      
        
        instance.setOperador(null);
        assertEquals(0.0, instance.bonusOperacao(), 0);
        
        instance.setOperador(EOperator.soma);
        assertEquals(0.1, instance.bonusOperacao(), 0);
        
        instance.setOperador(EOperator.subtracao);
        assertEquals(0.2, instance.bonusOperacao(), 0);
        
        instance.setOperador(EOperator.multiplicacao);
        assertEquals(0.5, instance.bonusOperacao(), 0);
        
        instance.setOperador(EOperator.divisao);
        assertEquals(0.9, instance.bonusOperacao(), 0);
        
        
    }
    
    
}
