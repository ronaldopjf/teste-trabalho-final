/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.domain;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author iranf
 */
public class PartidaTest {
    
    public PartidaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void addBonusTest() {
        Partida instance = new Partida();
        instance.setBonificacao(5.0);
        instance.addBonus(10.0);
        assertEquals(15, instance.getBonificacao(), 0);

    }

    @Test
    public void removeBonusTest() {
        Partida instance = new Partida();
        instance.setBonificacao(15.0);
        instance.removeBonus(10.0);
        assertEquals(5, instance.getBonificacao(), 0);

    }
    
    @Test
    public void construtorVazioTest() {
        Partida instance = new Partida();
        
        assertNotNull(instance);
        assertNull(instance.getJogoList());
        assertEquals(0, instance.getBonificacao(), 0);
        assertEquals("0,00", instance.getBonificacaoFormatada());
        assertNull(instance.getData());
        assertNull(instance.getId());
        assertEquals(0, instance.getTempo());
        assertNull(instance.getUsuario());

    }
    
    @Test
    public void construtorIdTest() {
        Partida instance = new Partida(5);
        
        assertNotNull(instance);
        assertNull(instance.getJogoList());
        assertEquals(0, instance.getBonificacao(), 0);
        assertEquals("0,00",instance.getBonificacaoFormatada());
        assertNull(instance.getData());
        assertEquals(0, instance.getTempo());
        assertNull(instance.getUsuario());
        assertEquals("5", instance.getId().toString());

    }
    
    @Test
    public void construtorCheioTest() {
        Partida instance = new Partida(5, new Date(), 20.0, Long.parseLong("12"));
        
        instance.setUsuario(new Usuario(5, "Pedrin", "pedrin", "pedrin@gmail.com", "pedriN@123", "Juiz de Fora", 
                new Date()));
        
        instance.setJogoList(new ArrayList<>());
        instance.getJogoList().add(new Jogo(1, 10, 5, EOperator.soma, 15, 20, 5));
        instance.getJogoList().add(new Jogo(1, 15, 10, EOperator.subtracao, 5, 5, 10));
        
        assertNotNull(instance);
        assertEquals(1, instance.getAcertos());
        assertEquals(20.0, instance.getBonificacao(), 0);
        assertEquals("20,00", instance.getBonificacaoFormatada());
        assertEquals(new Date().toString(), instance.getData().toString());
        assertEquals(new SimpleDateFormat("dd/MM/yyyy").format(new Date()), instance.getDataFormatada());
        assertEquals(1, instance.getErros());
        assertEquals(Long.parseLong("12"), instance.getTempo());
        assertEquals("5", instance.getId().toString());
        
        assertNotNull(instance.getUsuario());
        assertEquals("5", instance.getUsuario().getId().toString());
        assertEquals("Pedrin", instance.getUsuario().getNome());
        
        assertNotNull(instance.getJogoList());
        assertEquals(2, instance.getJogoList().size());
        assertEquals(EOperator.soma, instance.getJogoList().get(0).getOperador());
        assertEquals(EOperator.subtracao, instance.getJogoList().get(1).getOperador());

    }
    
    @Test
    public void getSetBasicoTest() {
        Partida instance = new Partida();
        
        instance.setId(5);
        instance.setData(new Date());
        instance.setBonificacao(20.0);
        instance.setTempo(Long.parseLong("12"));
        
        instance.setUsuario(new Usuario(5, "Pedrin", "pedrin", "pedrin@gmail.com", "pedriN@123", "Juiz de Fora", 
                new Date()));
        
        instance.setJogoList(new ArrayList<>());
        instance.getJogoList().add(new Jogo(1, 10, 5, EOperator.soma, 15, 20, 5));
        instance.getJogoList().add(new Jogo(1, 15, 10, EOperator.subtracao, 5, 5, 10));
        
        assertNotNull(instance);
        assertEquals(1, instance.getAcertos());
        assertEquals(20.0, instance.getBonificacao(), 0);
        assertEquals("20,00", instance.getBonificacaoFormatada());
        assertEquals(new Date().toString(), instance.getData().toString());
        assertEquals(new SimpleDateFormat("dd/MM/yyyy").format(new Date()), instance.getDataFormatada());
        assertEquals(1, instance.getErros());
        assertEquals(Long.parseLong("12"), instance.getTempo());
        assertEquals("5", instance.getId().toString());
        
        assertNotNull(instance.getUsuario());
        assertEquals("5", instance.getUsuario().getId().toString());
        assertEquals("Pedrin", instance.getUsuario().getNome());
        
        assertNotNull(instance.getJogoList());
        assertEquals(2, instance.getJogoList().size());
        assertEquals(EOperator.soma, instance.getJogoList().get(0).getOperador());
        assertEquals(EOperator.subtracao, instance.getJogoList().get(1).getOperador());

    }
    
}
