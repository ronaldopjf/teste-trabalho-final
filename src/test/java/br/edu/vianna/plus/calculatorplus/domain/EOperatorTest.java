/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.vianna.plus.calculatorplus.domain;

import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author iranf
 */
public class EOperatorTest {
    
    public EOperatorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    
    @Test
    public void getOperadorTest() {

        assertEquals("+", EOperator.soma.getOperador());
        assertEquals("-", EOperator.subtracao.getOperador());
        assertEquals("*", EOperator.multiplicacao.getOperador());
        assertEquals("/", EOperator.divisao.getOperador());

    }
    
    @Test
    public void valueOfStringTest(){
        
        assertEquals(EOperator.soma, EOperator.valueOf("soma"));
        assertEquals(EOperator.subtracao, EOperator.valueOf("subtracao"));
        assertEquals(EOperator.multiplicacao, EOperator.valueOf("multiplicacao"));
        assertEquals(EOperator.divisao, EOperator.valueOf("divisao"));
    }
    
    @Test
    public void calcularSomaTest() {

        assertEquals(20, EOperator.soma.calcular(10, 10), 0);
        assertEquals(15, EOperator.soma.calcular(5, 10), 0);
        assertEquals(15, EOperator.soma.calcular(10, 5), 0);
        assertEquals(0, EOperator.soma.calcular(-10, 10), 0);
        assertEquals(-20, EOperator.soma.calcular(-10, -10), 0);
        assertEquals(10, EOperator.soma.calcular(0, 10), 0);
        assertEquals(-10, EOperator.soma.calcular(0, -10), 0);
        assertEquals(10, EOperator.soma.calcular(10, 0), 0);
        assertEquals(-10, EOperator.soma.calcular(-10, 0), 0);
    }
    
    @Test
    public void calcularSubtracaoTest() {

        assertEquals(0, EOperator.subtracao.calcular(10, 10), 0);
        assertEquals(-5, EOperator.subtracao.calcular(5, 10), 0);
        assertEquals(5, EOperator.subtracao.calcular(10, 5), 0);
        assertEquals(-20, EOperator.subtracao.calcular(-10, 10), 0);
        assertEquals(0, EOperator.subtracao.calcular(-10, -10), 0);
        assertEquals(-10, EOperator.subtracao.calcular(0, 10), 0);
        assertEquals(10, EOperator.subtracao.calcular(0, -10), 0);
        assertEquals(10, EOperator.subtracao.calcular(10, 0), 0);
        assertEquals(-10, EOperator.subtracao.calcular(-10, 0), 0);
    }
    
    @Test
    public void calcularMultiplicacaoTest() {

        assertEquals(100, EOperator.multiplicacao.calcular(10, 10), 0);
        assertEquals(50, EOperator.multiplicacao.calcular(5, 10), 0);
        assertEquals(50, EOperator.multiplicacao.calcular(10, 5), 0);
        assertEquals(-100, EOperator.multiplicacao.calcular(-10, 10), 0);
        assertEquals(100, EOperator.multiplicacao.calcular(-10, -10), 0);
        assertEquals(0, EOperator.multiplicacao.calcular(0, 10), 0);
        assertEquals(0, EOperator.multiplicacao.calcular(0, -10), 0);
        assertEquals(0, EOperator.multiplicacao.calcular(10, 0), 0);
        assertEquals(0, EOperator.multiplicacao.calcular(-10, 0), 0);
    }
    
    @Test
    public void calcularDivisaoTest() {

        assertEquals(1, EOperator.divisao.calcular(10, 10), 0);
        assertEquals(0, EOperator.divisao.calcular(5, 10), 0);
        assertEquals(2, EOperator.divisao.calcular(10, 5), 0);
        assertEquals(-1, EOperator.divisao.calcular(-10, 10), 0);
        assertEquals(1, EOperator.divisao.calcular(-10, -10), 0);
        assertEquals(0, EOperator.divisao.calcular(0, 10), 0);
        assertEquals(0, EOperator.divisao.calcular(0, -10), 0);
    }
}
