/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Integracao;

import br.edu.vianna.plus.calculatorplus.dao.PartidaDao;
import br.edu.vianna.plus.calculatorplus.dao.UsuarioDao;
import br.edu.vianna.plus.calculatorplus.domain.Partida;
import br.edu.vianna.plus.calculatorplus.domain.Usuario;
import db.util.DbUnitHelper;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author rim
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IntegracaoPartidaTest {

    private static DbUnitHelper helper;
    private static String xmlPath = "src/test/java/xml/";

    public IntegracaoPartidaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        helper = new DbUnitHelper(xmlPath);
    }

    @AfterClass
    public static void tearDownClass() {
        helper.importar(xmlPath + "Bkp-original.xml");
    }

    @Before
    public void setUp() {
        helper.importar(xmlPath + "baseTeste.xml");
    }

    @After
    public void tearDown() {
    }

    @Test
    public void getAllBonusTest() {

        double qdadeBonusPartida = new PartidaDao().getAllBonus();

        assertEquals(190.5, qdadeBonusPartida, 0);
    }

    @Test
    public void getMinhasPartidasTest() {

        Usuario user = new UsuarioDao().findByLoginAndSenha("ana", "123456");

        List<Partida> partidas = new PartidaDao().getMinhasPartidas(user);

        assertEquals(1, partidas.size());
    }

    @Test
    public void userJaCompetiuHojeTrueTest() throws SQLException {

        Usuario user = new UsuarioDao().findByLoginAndSenha("ana", "123456");
        
        new PartidaDao().iniciarPartida(user);

        assertEquals(true, new PartidaDao().userJaCompetiuHoje(user));
    }

    @Test
    public void userJaCompetiuHojeFalseTest() {

        Usuario user = new UsuarioDao().findByLoginAndSenha("joao", "123456");

        assertEquals(false, new PartidaDao().userJaCompetiuHoje(user));
    }

    @Test
    public void getIniciarPartidaTest() throws SQLException {

        Usuario user = new UsuarioDao().findByLoginAndSenha("maria", "123456");

        Partida part = new PartidaDao().iniciarPartida(user);

        assertEquals(part.getUsuario().getId(), user.getId());
        assertEquals(part.getUsuario().getLogin(), "maria");
        assertEquals(part.getUsuario().getLogin(), user.getLogin());
    }
    
    @Test
    public void getPartidaIniciadaTest() throws SQLException {

        Usuario user = new UsuarioDao().findByLoginAndSenha("maria", "123456");
        
        new PartidaDao().iniciarPartida(user);

        Partida part = new PartidaDao().getPartidaIniciada(user);

        assertEquals(part.getUsuario().getId(), user.getId());
        assertEquals(part.getUsuario().getLogin(), "maria");
        assertEquals(part.getUsuario().getLogin(), user.getLogin());
    }
    
    @Test
    public void savePartidaTest() {

        Partida part = new Partida(null, new Date(), 45.5, Long.parseLong("20"));
        part.setUsuario(new UsuarioDao().findByLoginAndSenha("joao", "123456"));

        new PartidaDao().save(part);

        Partida novaPart = new PartidaDao().getPartidaIniciada(part.getUsuario());

        Assert.assertNotNull(novaPart);
        Assert.assertNotNull(novaPart.getId());
        assertEquals(novaPart.getData().toString(), new Date().toString());
        assertEquals(novaPart.getBonificacao(), 45.5, 0);
        assertEquals(novaPart.getTempo(), Long.parseLong("20"));
    }
}
