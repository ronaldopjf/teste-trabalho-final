/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Integracao;

import br.edu.vianna.plus.calculatorplus.dao.UsuarioDao;
import br.edu.vianna.plus.calculatorplus.domain.Usuario;
import db.util.DbUnitHelper;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author Ronaldo
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IntegracaoUsuarioTest {

    private static DbUnitHelper helper;
    private static String xmlPath = "src/test/java/xml/";

    public IntegracaoUsuarioTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        System.out.println("Antes de Rodar todos os testes - Before Class");
        helper = new DbUnitHelper(xmlPath);
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("Depois de Rodar todos os testes - After Class");
        helper.importar(xmlPath + "Bkp-original.xml");
    }

    @Before
    public void setUp() {
        System.out.println("Antes de Rodar 1 teste - Before");
        helper.importar(xmlPath + "baseTeste.xml");
    }

    @After
    public void tearDown() {
        System.out.println("Depois de Rodar 1 teste - After");
    }

    /**
     * Test testMain1, limpa o banco, insere a baseTeste.xml e verifica se a
     * qdade de users é igual a 1.
     */
    @Test
    public void getAllUsersTest() {

        long qdadeUser = new UsuarioDao().getAllUsers();

        assertEquals(3, qdadeUser);
    }

    /**
     * Test testMain2, adiciona mais um usuário ao banco e verifica se a qdade
     * de users é igual a 2.
     */
    @Test
    public void saveUserTest() {

        UsuarioDao userDao = new UsuarioDao();
        Usuario user = new Usuario(null, "José Antônio", "zezin", "zezin@gmail.com", "123456", "Juiz de Fora", new Date());
        userDao.save(user);
        long qdadeUser = userDao.getAllUsers();

        assertEquals(4, qdadeUser);
    }

    /**
     * Test testMain3, busca por login e senha.
     */
    @Test
    public void findByLoginAndSenhaTest() {

        Usuario user = new UsuarioDao().findByLoginAndSenha("ana", "123456");

        assertEquals("Ana Madalena", user.getNome());
    }

    /**
     * Test testMain4, findByLoginAndSenhaNullTest
     */
    @Test
    public void findByLoginAndSenhaNullTest() {

        Usuario user = new UsuarioDao().findByLoginAndSenha("asd", "345987");

        assertNull(user);
    }

    /**
     * Test testMain5, editUserTest
     */
    @Test
    public void editUserTest() {

        Usuario user = new UsuarioDao().findByLoginAndSenha("joao", "123456");
        user.setLogin("joao123");
        new UsuarioDao().save(user);

        user = new UsuarioDao().findByLoginAndSenha("joao123", "123456");

        assertEquals(user.getNome(), "João Roberto");
    }
}
