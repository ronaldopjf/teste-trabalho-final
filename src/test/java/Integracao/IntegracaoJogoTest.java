/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Integracao;

import br.edu.vianna.plus.calculatorplus.dao.JogoDao;
import br.edu.vianna.plus.calculatorplus.domain.Partida;
import db.util.DbUnitHelper;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author rim
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IntegracaoJogoTest {

    private static DbUnitHelper helper;
    private static String xmlPath = "src/test/java/xml/";

    public IntegracaoJogoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        helper = new DbUnitHelper(xmlPath);
    }

    @AfterClass
    public static void tearDownClass() {
        helper.importar(xmlPath + "Bkp-original.xml");
    }

    @Before
    public void setUp() {
        helper.importar(xmlPath + "baseTeste.xml");
    }

    @After
    public void tearDown() {
    }

    @Test
    public void getAllAcertosTest() {

        long qdadeAcertos = new JogoDao().getAllAcertos();

        assertEquals(1, qdadeAcertos);
    }

    @Test
    public void getAllErrosTest() {

        long qdadeErros = new JogoDao().getAllErros();

        assertEquals(3, qdadeErros);
    }

    @Test
    public void getAllAcertosUsuarioTest() {

        long qdadeAcertos = new JogoDao().getAllAcertos(1);

        assertEquals(1, qdadeAcertos);
    }

    @Test
    public void saveRespostaUserTest() {

        Partida p = new JogoDao().saveRespostaUser(1, 45);

        assertEquals("1", p.getJogoList().get(0).getIdjogo().toString());
        assertEquals(45, p.getJogoList().get(0).getResposta(), 0);
    }

}
