package db.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;

public class DbUnitHelper {

    private Connection conexao;
    private DatabaseConnection conexaoDBUnit;
    private String xmlFolder;
    EntityManager em;

    public DbUnitHelper(String xmlFolder) {

        this.xmlFolder = xmlFolder;

        try {
            EntityManagerFactory emf = Persistence.createEntityManagerFactory("PUcalculatorPlus");
            em = emf.createEntityManager();

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conexao = DriverManager
                    .getConnection("jdbc:mysql://mysql-db:3306/calculator", "root", "123456789");
            conexaoDBUnit = new DatabaseConnection(conexao);
            DatabaseConfig config = conexaoDBUnit.getConfig();
            config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());

            backupDataBase();

        } catch (Exception e) {
            throw new RuntimeException("Erro inicializando DBUnit", e);
        }
    }

//    public void execute(DatabaseOperation operation, String xml) {
//        try {
//            InputStream is = getClass().getResourceAsStream("/" + xmlFolder + "/" + xml);
//            FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
//            IDataSet dataSet = builder.build(is);
//
//            operation.execute(conexaoDBUnit, dataSet);
//        } catch (Exception e) {
//            throw new RuntimeException("Erro executando DbUnit", e);
//        }
//    }
    public void backupDataBase() {
        try {
            IDataSet dataSet = conexaoDBUnit.createDataSet();
            new FileOutputStream(xmlFolder + "Bkp.xml", false).close();
            FlatXmlDataSet.write(dataSet, new FileOutputStream(xmlFolder + "Bkp.xml"));
        } catch (Exception e) {
            throw new RuntimeException("Erro executando DbUnit", e);
        }
    }

    public void RestoreDataBase() {
        try {
            IDataSet ids = conexaoDBUnit.createDataSet();
            DatabaseOperation.DELETE_ALL.execute(conexaoDBUnit, ids);

            FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
            IDataSet dataSet = builder.build(new FileInputStream(xmlFolder + "Bkp.xml"));
            DatabaseOperation.INSERT.execute(conexaoDBUnit, dataSet);

        } catch (Exception e) {
            throw new RuntimeException("Erro Restore DbUnit", e);
        }
    }

    public void close() {
        try {
            conexaoDBUnit.close();
            conexao.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void importar(String file) {
        try {
            String[] table = {"jogo"};
            IDataSet ids = conexaoDBUnit.createDataSet(table);
            DatabaseOperation.DELETE.execute(conexaoDBUnit, ids);

            table = new String[]{"partida"};
            ids = conexaoDBUnit.createDataSet(table);
            DatabaseOperation.DELETE.execute(conexaoDBUnit, ids);

            table = new String[]{"usuario"};
            ids = conexaoDBUnit.createDataSet(table);
            DatabaseOperation.DELETE.execute(conexaoDBUnit, ids);

            //DatabaseOperation.DELETE_ALL.execute(conexaoDBUnit, ids);
            FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
            IDataSet dataSet = builder.build(new FileInputStream(file));
            DatabaseOperation.INSERT.execute(conexaoDBUnit, dataSet);

        } catch (Exception e) {
            throw new RuntimeException("Erro Restore DbUnit", e);
        }
    }

}
