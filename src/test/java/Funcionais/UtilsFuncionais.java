/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Funcionais;

/**
 *
 * @author iranf
 */
public class UtilsFuncionais {
    
    //URL para uso local no Netbeans.
    //static String URL_TOMCAT = "http://localhost:8084/calculatorPlus/";
    
    //URL para uso no Tomcat do Docker localmente.
    //static String URL_TOMCAT = "http://localhost:8888/calculatorPlus/";
    
    //URL para uso no Jenkins: De Container para Container. 
    static String URL_TOMCAT = "http://tomcat:8080/calculatorPlus/";

    public UtilsFuncionais() {
    }
}
