/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Funcionais;

// Generated by Selenium IDE
import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import static org.hamcrest.CoreMatchers.is;
import org.junit.BeforeClass;
import org.junit.Ignore;

/**
 *
 * @author iranf
 */

public class PermissoesAcessoTest {
    
  private WebDriver driver;
  
  @BeforeClass
  public static void setupClass(){
      WebDriverManager.chromedriver().setup();
  }
  @Before
  public void setUp() {
    driver = new ChromeDriver();
  }
  @After
  public void tearDown() {
    driver.quit();
  }
  
  /* #####  IMPORTANTE  #####*/
  
  /*Ignorado devido a máquina não estar aguentando o volume de testes funcionais, travando a máquina.
  Pois a maioria dos testes funcionais depende de "notificações" surgindo na tela, como o navegador 
  trava no funcionamento, os testes estão falhando devido a isso. Rodando somente os testes funcionais
  todos funcionam corretamente. Todos os testes da classe que estão ignorados estão nessa situação.*/
  
  @Ignore 
  @Test
  public void acessoNaoPermitidoCompeticaoTest() {
    driver.get(UtilsFuncionais.URL_TOMCAT + "?ac=competicao");
    //driver.findElement(By.cssSelector(".col-11")).click();
    assertThat(driver.findElement(By.cssSelector(".navbar-brand")).getText(), is("Crie Seu Cadastro"));
    assertThat(driver.findElement(By.cssSelector(".navbar-brand")).getText(), is("Crie Seu Cadastro"));
    assertThat(driver.findElement(By.cssSelector("span:nth-child(4)")).getText(), is("login necessário"));
    driver.findElement(By.cssSelector(".logo-normal")).click();
  }
  
  @Test
  public void menuSemLoginTest() {
    driver.get(UtilsFuncionais.URL_TOMCAT);
    driver.findElement(By.cssSelector(".navbar > .container-fluid")).click();
    assertThat(driver.findElement(By.cssSelector(".navbar-brand")).getText(), is("Crie Seu Cadastro"));
    driver.findElement(By.cssSelector(".sidebar-wrapper")).click();
    assertThat(driver.findElement(By.cssSelector("li:nth-child(3) p")).getText(), is("CADASTRO"));
    driver.findElement(By.cssSelector("li:nth-child(2)")).click();
    assertThat(driver.findElement(By.cssSelector("li:nth-child(2) p")).getText(), is("LOGIN"));
    driver.findElement(By.cssSelector(".sidebar-wrapper")).click();
  }
  
  @Test
  public void menuComLoginTest() {
    driver.get(UtilsFuncionais.URL_TOMCAT);
    driver.findElement(By.cssSelector("li:nth-child(2) p")).click();
    driver.findElement(By.name("cpLogin")).click();
    driver.findElement(By.name("cpLogin")).sendKeys("pedrin");
    driver.findElement(By.name("cpSenha")).sendKeys("123456");
    driver.findElement(By.cssSelector(".btn")).click();
    driver.findElement(By.cssSelector(".navbar-wrapper")).click();
    assertThat(driver.findElement(By.linkText("Bem Vindo, Pedrin")).getText(), is("Bem Vindo, Pedrin"));
    assertThat(driver.findElement(By.cssSelector("li:nth-child(4) p")).getText(), is("SAIR"));
    driver.findElement(By.cssSelector("li:nth-child(3)")).click();
    assertThat(driver.findElement(By.cssSelector("li:nth-child(3) p")).getText(), is("RANKING"));
    driver.findElement(By.cssSelector("li:nth-child(2)")).click();
    assertThat(driver.findElement(By.cssSelector("li:nth-child(2) p")).getText(), is("COMPETIÇÃO"));
    driver.findElement(By.cssSelector("li:nth-child(4) p")).click();
  }
  
  @Test
  public void acessoPermitidoCompeticaoTest() {
    driver.get(UtilsFuncionais.URL_TOMCAT);
    driver.findElement(By.cssSelector("li:nth-child(2) p")).click();
    driver.findElement(By.name("cpLogin")).click();
    driver.findElement(By.name("cpLogin")).sendKeys("pedrin");
    driver.findElement(By.name("cpSenha")).sendKeys("123456");
    driver.findElement(By.cssSelector(".btn")).click();
    driver.findElement(By.cssSelector("li:nth-child(2) p")).click();
    driver.findElement(By.cssSelector(".active p")).click();
    driver.findElement(By.cssSelector(".card-title")).click();
    assertThat(driver.findElement(By.cssSelector(".card-title")).getText(), is("Minhas Competições"));
    driver.findElement(By.cssSelector("li:nth-child(4) p")).click();
    driver.findElement(By.cssSelector(".logo-normal")).click();
  }  
  
  @Test
  public void acessoPermitidoRankingTest() {
    driver.get(UtilsFuncionais.URL_TOMCAT);
    driver.findElement(By.cssSelector("li:nth-child(2) p")).click();
    driver.findElement(By.name("cpLogin")).click();
    driver.findElement(By.name("cpLogin")).sendKeys("pedrin");
    driver.findElement(By.name("cpSenha")).sendKeys("123456");
    driver.findElement(By.cssSelector(".btn")).click();
    driver.findElement(By.cssSelector("li:nth-child(3) p")).click();
    driver.findElement(By.cssSelector(".card-title")).click();
    assertThat(driver.findElement(By.cssSelector(".card-title")).getText(), is("Ranking Geral"));
    driver.findElement(By.cssSelector("li:nth-child(4) p")).click();
    driver.findElement(By.cssSelector(".logo-normal")).click();
  }
  
  @Ignore
  @Test
  public void acessoNaoPermitidoRankingTest() {
    driver.get(UtilsFuncionais.URL_TOMCAT + "?ac=ranking");
    driver.findElement(By.cssSelector("span:nth-child(4)")).click();
    assertThat(driver.findElement(By.cssSelector("span:nth-child(4)")).getText(), is("login necessário"));
    driver.findElement(By.cssSelector(".logo-normal")).click();
  }
  
  @Ignore
  @Test
  public void acessoNaoPermitidoDetalheCompeticaoTest() {
    driver.get(UtilsFuncionais.URL_TOMCAT + "?ac=detalhesPart&id=13");
    assertThat(driver.findElement(By.cssSelector(".navbar-brand")).getText(), is("Crie Seu Cadastro"));
    assertThat(driver.findElement(By.cssSelector(".navbar-brand")).getText(), is("Crie Seu Cadastro"));                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
    assertThat(driver.findElement(By.cssSelector("span:nth-child(4)")).getText(), is("login necessário"));
    driver.findElement(By.cssSelector(".logo-normal")).click();
  }
  
  @Test
  public void acessoPermitidoDetalhesCompeticaoTest() {
    driver.get(UtilsFuncionais.URL_TOMCAT);
    driver.findElement(By.cssSelector("li:nth-child(2) p")).click();
    driver.findElement(By.name("cpLogin")).click();
    driver.findElement(By.name("cpLogin")).sendKeys("pedrin");
    driver.findElement(By.name("cpSenha")).sendKeys("123456");
    driver.findElement(By.cssSelector(".btn")).click();
    driver.findElement(By.cssSelector("li:nth-child(2) p")).click();
    driver.findElement(By.cssSelector("td:nth-child(1)")).click();
    assertThat(driver.findElement(By.cssSelector("td:nth-child(1)")).getText(), is("06/12/2019"));
    driver.findElement(By.cssSelector("td:nth-child(3)")).click();
    assertThat(driver.findElement(By.cssSelector("td:nth-child(3)")).getText(), is("1"));
    driver.findElement(By.cssSelector("td:nth-child(4)")).click();
    assertThat(driver.findElement(By.cssSelector("td:nth-child(4)")).getText(), is("9"));
    driver.findElement(By.cssSelector("td:nth-child(4)")).click();
    driver.findElement(By.cssSelector(".nc-alert-circle-i")).click();
    driver.findElement(By.cssSelector(".card-user > .card-body")).click();
    assertThat(driver.findElement(By.cssSelector(".title")).getText(), is("06/12/2019"));
    driver.findElement(By.cssSelector(".text-center:nth-child(3)")).click();
    assertThat(driver.findElement(By.cssSelector(".text-center:nth-child(3)")).getText(), is("Acertos :: 1\nErros :: 9\nPontuação :: Cr$ 0,00"));
    driver.findElement(By.cssSelector("li:nth-child(1) p")).click();
    driver.findElement(By.cssSelector("li:nth-child(4) p")).click();
  }
}
